package com.edgenda.bnc.eventsmanager.repository;

import com.edgenda.bnc.eventsmanager.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    Optional<Event> findById(Long id);

    @Query("SELECT event FROM Event event JOIN event.guests guest WHERE guest.id = ?1")
    List<Event> findByGuestId(Long guestId);
    
    @Query("select event from Event event join event.guests guest where guest.id = :id and (event.startDate between :startDate and :endDate or event.endDate between :startDate and :endDate)")
	List<Event> findByGuestIdAndPeriod(@Param("id")Long id, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
    
}
