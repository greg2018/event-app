package com.edgenda.bnc.eventsmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Event {

    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    Date endDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "GUESTS_EVENTS")
    @JsonIgnoreProperties("events")
    private List<Guest> guests;

    public Event() {
    }

    public Event(Long id, String name, String status, List<Guest> guests) {
        this.id = id;
        this.name = name;
        this.startDate = new Date();
        this.status = status;
        this.guests = guests;
    }

    public Event(Long id, String name, String status, Date startDate, Date endDate, List<Guest> guests) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.guests = guests;
    }

    @PersistenceConstructor
    public Event(String name, String status, Date startDate, Date endDate, List<Guest> guests) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
        this.guests = guests;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
