package com.edgenda.bnc.eventsmanager.rest;

import com.edgenda.bnc.eventsmanager.model.Event;
import com.edgenda.bnc.eventsmanager.model.Guest;
import com.edgenda.bnc.eventsmanager.service.GuestService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/guests")
public class GuestController {

    private GuestService guestService;

    @Autowired
    public GuestController(GuestService guestService) {
        this.guestService = guestService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Guest> getGuests() {
        return guestService.getGuests();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value="Get a guest",response= Guest.class)
    @ApiResponses(value={
            @ApiResponse(code=200,message="Guest Details Retrieved",response=Guest.class),
            @ApiResponse(code=500,message="Internal Server Error"),
            @ApiResponse(code=404,message="No guest found")
    })
    public Guest getGuest(@PathVariable Long id) {
        return guestService.getGuest(id);
    }

/*    @RequestMapping(path = "/{id}/events", method = RequestMethod.GET)
    public List<Guest> getGuestWithEvents(@PathVariable Long id) {
        return guestService.getGuestEvents(id);
    }*/
}
