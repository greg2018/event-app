package com.edgenda.bnc.eventsmanager.rest;


import com.edgenda.bnc.eventsmanager.model.Event;
import com.edgenda.bnc.eventsmanager.model.Guest;
import com.edgenda.bnc.eventsmanager.service.EventService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/events")
@Api(value = "/events", description = "Events management", produces = "application/json")
public class EventController {

    private EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Get an event", response = Event.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Event Details Retrieved", response = Event.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "No event found")
    })
    public Event getEvent(@PathVariable Long id) {
        return eventService.getEvent(id);
    }

    @GetMapping
    @ApiOperation(value = "Get all events", response = Event.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Events Details Retrieved", response = Event.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public List<Event> getAllEvents() {
        return eventService.getEvents();
    }


    @ApiOperation(value = "Create an event", response = Event.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Event created", response = Event.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping
    public Event createEvent(@RequestBody Event event) {
        return eventService.createEvent(event);
    }

    @PutMapping(path = "/{id}")
    @ApiOperation(value = "Update an event", response = Event.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Event updated", response = Event.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public Event updateEvent(@PathVariable Long id, @RequestBody Event event) {
        Event innerEvent = new Event(id, event.getName(), event.getStatus(), event.getStartDate(), event.getEndDate(), event.getGuests());
        return eventService.updateEvent(innerEvent);
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Delete an event", response = Event.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Event updated", response = Event.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Event not found")
    })
    public void deleteEvent(@PathVariable Long id) {
        eventService.deleteEvent(id);
    }

    @DeleteMapping
    @ApiOperation(value = "Delete all events with canceled status", response = Event.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Events deleted", response = Event.class),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public void deleteEventsWithASpecificStatus(@RequestParam(required = true) String status) {
        eventService.deleteEventsWithASpecificStatus(status);
    }

    @GetMapping(path = "/{id}/guests")
    @ApiOperation(value = "Get an event guests list", response = Event.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Event guests list retrieved", response = Event.class),
            @ApiResponse(code = 500, message = "Internal Server Error"),
            @ApiResponse(code = 404, message = "Event not found")
    })
    public List<Guest> getEventGuests(@PathVariable Long id) {
        return eventService.getEventGuests(id);
    }

}
