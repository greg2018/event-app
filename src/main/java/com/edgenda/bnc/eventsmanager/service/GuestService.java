package com.edgenda.bnc.eventsmanager.service;

import com.edgenda.bnc.eventsmanager.model.Event;
import com.edgenda.bnc.eventsmanager.model.Guest;

import java.util.List;

public interface GuestService {

    public Guest getGuest(Long id);
    public Guest createGuest(Guest guest);
    public Guest updateGuest(Guest guest);
    public void deleteGuest(Long id);
    public List<Guest> getGuests();
    public List<Event> getGuestEvents(Long guestId);

}
