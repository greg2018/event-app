package com.edgenda.bnc.eventsmanager.service;

import com.edgenda.bnc.eventsmanager.model.Event;
import com.edgenda.bnc.eventsmanager.model.Guest;
import com.edgenda.bnc.eventsmanager.repository.EventRepository;
import com.edgenda.bnc.eventsmanager.repository.GuestRepository;
import com.edgenda.bnc.eventsmanager.service.exception.UnknownEventException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private GuestRepository guestRepository;

    @Override
    public Event getEvent(Long id) {
        Assert.notNull(id, "Event ID cannot be null");
        return eventRepository.findById(id)
                .orElseThrow(() -> new UnknownEventException(id));
    }

    @Override
    public List<Event> getEvents() {
        return eventRepository.findAll();
    }

    @Override
    public Event createEvent(Event event) {
        return eventRepository.saveAndFlush(event);
    }

    @Override
    public Event updateEvent(Event event) {
        return eventRepository.saveAndFlush(event);
    }

    @Override
    public void deleteEvent(Long id) {
    }

    @Override
    public void deleteEventsWithASpecificStatus(String status) {

    }

    @Override
    public List<Guest> getEventGuests(Long eventId) {
    	
    	return eventRepository.findById(eventId).map(event -> event.getGuests()).orElseThrow(() -> new UnknownEventException(eventId));
    }

    @Override
    public List<Event> getEventsForAPeriod(Long guestId, Date startDate, Date endDate) {
        return eventRepository.findByGuestIdAndPeriod(guestId, startDate, endDate);
    }


    //@Autowired
    //private final EmployeeRepository employeeRepository;

    //@Autowired
    //private final SkillRepository skillRepository;

    /*


    public Employee getEmployee(Long id) {
        Assert.notNull(id, "Employee ID cannot be null");
        return employeeRepository.findById(id)
                .orElseThrow(() -> new UnknownEmployeeException(id));
    }

    public List<Employee> getEmployees() {
        return employeeRepository.findAll();
    }

    public Employee createEmployee(Employee employee) {
        Assert.notNull(employee, "Employee cannot be null");
        final Employee newEmployee = new Employee(
                employee.getFirstName(),
                employee.getLastName(),
                employee.getEmail(),
                Collections.emptyList()
        );
        return employeeRepository.save(newEmployee);
    }

    public void updateEmployee(Employee employee) {
        Assert.notNull(employee, "Employee cannot be null");
        this.getEmployee(employee.getId());
        employeeRepository.save(employee);
    }

    public List<Skill> getEmployeeSkills(Long employeeId) {
        return skillRepository.findByEmployeeId(employeeId);
    }

    public void deleteEmployee(Long id) {
        Assert.notNull(id, "ID cannot be null");
        employeeRepository.delete(id);
    }
    */
}
