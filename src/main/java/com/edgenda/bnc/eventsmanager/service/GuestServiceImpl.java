package com.edgenda.bnc.eventsmanager.service;

import com.edgenda.bnc.eventsmanager.model.Event;
import com.edgenda.bnc.eventsmanager.model.Guest;
import com.edgenda.bnc.eventsmanager.repository.GuestRepository;
import com.edgenda.bnc.eventsmanager.service.exception.UnknownGuestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class GuestServiceImpl implements GuestService {

    @Autowired
    private GuestRepository guestRepository;

    @Override
    public Guest getGuest(Long id) {
        Assert.notNull(id, "Guest ID cannot be null");
        return guestRepository.findById(id)
                .orElseThrow(() -> new UnknownGuestException(id));
    }

    @Override
    public Guest createGuest(Guest guest) {
        return null;
    }

    @Override
    public Guest updateGuest(Guest guest) {
        return null;
    }

    @Override
    public void deleteGuest(Long id) {
    }

    @Override
    public List<Guest> getGuests() {
        return null;
    }

    @Override
    public List<Event> getGuestEvents(Long guestId) {
        return null;
    }


    /*
    private final SkillRepository skillRepository;

    private final EmployeeRepository employeeRepository;

    @Autowired
    public GuestServiceImpl(SkillRepository skillRepository, EmployeeRepository employeeRepository) {
        this.skillRepository = skillRepository;
        this.employeeRepository = employeeRepository;
    }

    public Skill getSkill(Long id) {
        Assert.notNull(id, "Skill ID cannot be null");
        return skillRepository.findById(id)
                .orElseThrow(() -> new UnknownSkillException(id));
    }

    public List<Skill> getSkills() {
        return skillRepository.findAll();
    }

    public List<Employee> getEmployeesWithSkill(Long skillId) {
        Assert.notNull(skillId, "Skill ID cannot be null");
        return employeeRepository.findBySkillId(skillId);
    }
    */
}

