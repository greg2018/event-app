package com.edgenda.bnc.eventsmanager.service;

import com.edgenda.bnc.eventsmanager.model.Event;
import com.edgenda.bnc.eventsmanager.model.Guest;

import java.util.Date;
import java.util.List;

public interface EventService {

    public Event getEvent(Long id);
    public List<Event> getEvents();
    public Event createEvent(Event event);
    public Event updateEvent(Event event);
    public void deleteEvent(Long id);
    public void deleteEventsWithASpecificStatus(String status);
    public List<Guest> getEventGuests(Long eventId);
    public List<Event>getEventsForAPeriod(Long guesId,Date startDate, Date endDate);
}
