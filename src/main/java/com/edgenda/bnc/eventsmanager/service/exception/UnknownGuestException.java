package com.edgenda.bnc.eventsmanager.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UnknownGuestException extends RuntimeException {

    public UnknownGuestException(Long id) {
        super("Unknown Guest with ID=" + id);
    }

}
