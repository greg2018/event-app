package com.edgenda.bnc.eventsmanager.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@Sql(scripts ="classpath:import_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@TestPropertySource(locations = "classpath:application_test.properties")
public class EventControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
    }

    @Test
    public void when_Calling_Events_Path_Then_Response_Code_Is_Ok() throws Exception {
        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get("/events"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void when_Calling_Unknown_Id_Event_Then_Response_Code_Is_NotFound() throws Exception {
        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get("/events/1"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void when_Calling_Known_Id_Event_Then_Response_Is_Ok_And_Values_Are_Ok() throws Exception {
        MvcResult result = mockMvc
                .perform(MockMvcRequestBuilders.get("/events/33"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id", is(33)))
                .andExpect(jsonPath("status", is("ACTIVE")))
                .andReturn();
    }
}

