package com.edgenda.bnc.eventsmanager.repository;

import com.edgenda.bnc.eventsmanager.model.Event;
import com.edgenda.bnc.eventsmanager.model.Guest;

import com.edgenda.bnc.eventsmanager.service.exception.UnknownEventException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(value = SpringRunner.class)
@SpringBootTest
@Sql(scripts = "classpath:import_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@TestPropertySource(locations = "classpath:application_test.properties")
public class EventRepositoryTest {

    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    @Autowired
    private EventRepository eventRepository;

    @Test
    public void repositoryNotNull() {
        assertNotNull("Event Repository not null", eventRepository);
    }

    @Test
    public void addEventTest() throws ParseException {
        Event event = new Event(new Long(1), "Test Event", "INACTIVE", dateFormatter.parse("2002-01-07 08:30:30"),
                dateFormatter.parse("2002-01-07 17:00:00"), null);
        Event savedEvent = eventRepository.saveAndFlush(event);
        Long id = savedEvent.getId();
        Optional<Event> optEvent = eventRepository.findById(new Long(id));
        assertEquals("Test Event", optEvent.get().getName());
    }

    @Test
    public void guestEvents() {
        List<Event> guestEvents = eventRepository.findByGuestId(new Long(55));
        assertEquals("Guest 55 has 3 events", 3, guestEvents.size());
        guestEvents.stream().map(Event::getName).forEach(System.out::println);
    }

    @Test
    public void removeEvent() {
        eventRepository.findAll().stream().map(Event::getName).forEach(System.out::println);
        Optional<Event> eventToRemove = eventRepository.findById(new Long(0));
        assertTrue("Event to remove present", eventToRemove.isPresent());
        Event event = eventToRemove.get();
        eventRepository.delete(event);
        Optional<Event> removedEvent = eventRepository.findById(new Long(0));
        assertFalse("Event 0 has been removed", removedEvent.isPresent());
    }

    @Test
    public void eventsOfOneGuest() {
        System.out.println("events of one guest");
        List<Event> events = eventRepository.findByGuestId(new Long(55));
        events.stream().map(Event::getId).forEach(System.out::println);
        assertEquals("number of events for a guest", 3, events.size());
    }

    @Test
    public void eventsOfGuestForSpecificPeriod() throws ParseException {
        Date startDate = dateFormatter.parse("2019-03-01 08:01:00");
        Date endDate = dateFormatter.parse("2019-03-04 17:00:00");
        Long id = 56L; // Louis Armstrong
        System.out.println("Louis");
        List<Event> events = eventRepository.findByGuestIdAndPeriod(id, startDate, endDate);
        assertEquals("Number events for a period", 2, events.size());
        assertEquals("Name of 1st event", "event 4", events.get(0).getName());
    }

    @Test
    public void listGuestsFromGivenEvent() {
        List<String> emails = Arrays.asList("pete@hotmail.com", "derekt@gmail.com", "wongwong@test.com");
        Long eventId = 33L;
        List<Guest> guests  = eventRepository.findById(eventId).map(e -> e.getGuests()).orElseThrow(() -> new UnknownEventException(eventId));
        assertEquals(3, guests.size());
        guests.stream().map(Guest::getEmail).forEach(System.out::println);
    }

    @Test
    public void updateAnEvent(){
        Long eventId = 33L;
        Optional<Event> eventOpt = eventRepository.findById(eventId);
        if(eventOpt.isPresent()){
            Event event = eventOpt.get();
            Event tmpEvent = new Event(eventId, event.getName() + " /modified", event.getStatus(), event.getStartDate(), event.getEndDate(), event.getGuests());
            Event modifiedEvent = eventRepository.saveAndFlush(tmpEvent);
            assertEquals("Modified event name", "event 1 /modified", modifiedEvent.getName());
            assertEquals("Event Id did not change",eventId,modifiedEvent.getId());
        }

    }
}