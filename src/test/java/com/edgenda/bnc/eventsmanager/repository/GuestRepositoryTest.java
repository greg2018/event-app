package com.edgenda.bnc.eventsmanager.repository;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(value = SpringRunner.class)
@SpringBootTest
public class GuestRepositoryTest {
	
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	
	@Autowired
	private GuestRepository guestRepository;

	@Test
	public void repositoryNotNull() {
		assertNotNull("Guest Repository not null", guestRepository);
	}

}