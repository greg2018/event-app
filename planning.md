event
-id long
-name string
-status string
-startdate timestamp
-enddate timestamp
-guests List<Guest>

guest
-id long 
-firstname String
-lastname String
-email String
-events List<Event>



taches
-entities
-interfaces 
  repo
  service
  
-implementations
  test
  repo
  service
  controller

create
update id
read 
  all
  id
delete
  id
  status